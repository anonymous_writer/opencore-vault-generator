#!/usr/bin/osascript
# Variablen
set OpenCoreEFIFolder to POSIX path of (choose folder with prompt "Bitte den OpenCore EFI Ordner w�hlen") as text
set OpenCoreEFIFolderScript to "\"" & OpenCoreEFIFolder & "OC\""

set ProgrammPath to POSIX path of ((path to me as text) & "::")
#set ProgrammPathScript to "\"" & ProgrammPath & "\""

set Step01 to "cd " & OpenCoreEFIFolderScript
set Step02 to "sh \"" & ProgrammPath & "CreateVault/create_vault.sh\"" & " \"" & OpenCoreEFIFolderScript & "\""
set Step03 to "\"" & ProgrammPath & "CreateVault/RsaTool\" -sign vault.plist vault.sig vault.pub"
set Step04 to "off=$(($(strings -a -t d OpenCore.efi | grep \"=BEGIN OC VAULT=\" | cut -f1 -d' ')+16))"
set Step05 to "dd of=OpenCore.efi if=vault.pub bs=1 seek=$off count=528 conv=notrunc"
set Step06 to "rm vault.pub"

#Programm
tell application "Terminal"
	activate
	do script Step01
	do script Step02 in window 1
	do script Step03 in window 1
	do script Step04 in window 1
	do script Step01 in window 1
	do script Step05 in window 1
	do script Step06 in window 1
end tell